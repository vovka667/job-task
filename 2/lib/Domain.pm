#!/usr/bin/env perl

package Domain;
our @EXPORT_OK = qw(get_domain_from_email convert_domain_to_unicode);
use base qw(Exporter);

use URI::UTF8::Punycode qw (puny_dec);
use Modern::Perl;

sub get_domain_from_email {
    my ($email) = @_;

    if ($email =~ /^[^@]+@([^@]+)$/) {
        return $1;
    }
    return "invalid";
}

sub convert_domain_to_unicode {
    my ($domain) = @_;

    my @labels;
    foreach my $label (split /\./, $domain) {
        if ($label =~ /^xn--/i) {
            $label = puny_dec($label);
            utf8::decode($label);
        }
        push @labels, $label;
    }
    return join ".", @labels;
}

1;

__END__

=encoding utf8

=head1 NAME

Domain

=head1 DESCRIPTION

Набор вспосогательных функций для работы с доменными именами.

=head1 METHODS

=head2 get_domain_from_email

Функция извлекает имя домена из адреса электронной почты. В случае неверного
адреса возвращается строка 'invalid'.

=head2 convert_domain_to_unicode

Функция, в случае необходимости, конвертирует имя домена из punycode.

=head1 AUTHOR

Vladimir N. Indik <vovka@vovka667.org>

#!/usr/bin/env perl

use Modern::Perl;
use open qw/:std :utf8/;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";
use Domain qw(get_domain_from_email convert_domain_to_unicode);

use Test::More tests => 14;
use Test::File;
use Test::LongString;

use_ok('URI::UTF8::Punycode');
use_ok('lib::abs', 'lib');
use_ok('Domain');

my $email = get_domain_from_email('nlmkOIM@asfd@fa');
is($email, 'invalid', 'nlmkOIM@asfd@fa is invalid mail address');

$email = get_domain_from_email('nknlknьrждлдь');
is($email, 'invalid', 'nknlknьrждлдь is invalid mail address');

my $domain = get_domain_from_email('вовка@xn--c1ad6a.xn--p1ai');
is($domain, 'xn--c1ad6a.xn--p1ai', '&domain_from_email works correct');

my $regrf = convert_domain_to_unicode($domain);
is($regrf, 'рег.рф', 'xn--c1ad6a.xn--p1ai and рег.рф are equivalent');

file_exists_ok("$Bin/../email_domains.pl");
file_not_empty_ok("$Bin/../email_domains.pl");
file_executable_ok("$Bin/../email_domains.pl");

file_exists_ok("$Bin/emails", 'emails list file exists');
file_not_empty_ok("$Bin/../t/emails");
file_readable_ok("$Bin/../t/emails");

# Проверяем соответствие вывода программы эталону.
my $standard_out = "рег.рф\t2
mail.ru\t2
иванов.рф\t1
rambler.ru\t1
vk.com\t1
localhost\t1
invalid\t1
";
my $out = `$Bin/../email_domains.pl $Bin/../t/emails`;
is_string($standard_out, $out, 'test run is ok');

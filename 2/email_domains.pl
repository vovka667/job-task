#!/usr/bin/env perl

use Modern::Perl;
use open qw/:std :utf8/;
use lib::abs 'lib';
use Domain qw(get_domain_from_email convert_domain_to_unicode);

# Счётчик вхождений для каждого домена
my %domain_cnt;

# Проходим STDIN построчно
while (my $line = <>) {
    chomp $line;
    my $domain = get_domain_from_email($line);
    $domain = lc $domain;
    $domain = convert_domain_to_unicode($domain);
    $domain_cnt{$domain}++;
}

# Сортируем домены по количеству вхождений и выводим на STDOUT
foreach my $domain ( sort { $domain_cnt{$b} <=> $domain_cnt{$a} } keys %domain_cnt ) {
    print $domain, "\t";
    print $domain_cnt{$domain}, "\n";
}

__END__

=encoding utf8

=head1 NAME

email_domains

=head1 SYNOPSIS

email_domains.pl [FILE]

=head1 DESCRIPTION

Программа считает количество вхождений доменов во входном потоке email
адресов и выводит отсортированный список доменов. На вход принимается
список email в кодировке utf8, по одному на строку, на выход подаётся
отсорированный в обратном порядке по количеству вхождений список.

=head1 AUTHOR

Vladimir N. Indik <vovka@vovka667.org>

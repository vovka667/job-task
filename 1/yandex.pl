#!/usr/bin/env perl

use strict;

# %desc - хеш дескрипторов файлов. Необходим для того,
#   чтобы не тратиться лишний раз на оперции отрытия/закрытия файлов.
my %desc;

# Читаем входной поток построчно
while (<>) {
    # Находим строки начала писем и извлекаем адрес
    if (my ($address) = /^From (\S+@\S+) /) {
        # В случае необходимости, создаём соответствующий файл
        if (not defined($desc{$address})) {
            open $desc{$address}, '>', "$address" or
                die "Can't create mailbox file: $!";
        }
        # Перенаправляем вывод в соответствующий файл
        select $desc{$address};
    }
    print;
}
